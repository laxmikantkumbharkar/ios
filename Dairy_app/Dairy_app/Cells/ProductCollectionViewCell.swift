//
//  ProductCollectionViewCell.swift
//  Dairy_app
//
//  Created by sunbeam on 20/07/19.
//  Copyright © 2019 sunbeam. All rights reserved.
//

import UIKit

class ProductCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewProduct: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelInfo: UILabel!
}
